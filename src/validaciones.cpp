/*
 * validaciones.cpp
 *
 *  Created on: 04/04/2018
 *      Author: patron
 */

#include <string>
#include "jugador.h"
#include <cstdlib>
#include "validaciones.h"

//luego quitar
#include <iostream>


bool validar_num(char* str, long int* num){
	char* error = NULL;
	long int num_aux = strtol(str, &error, 10);
	if (*error){
		return false;
	}
	else if (num_aux < 0){
		return false;
	}
	*num = num_aux;
	return true;
}

bool pedidoEsValido(long int pedidoNum){
	return(pedidoNum == SEMBRAR ||
			pedidoNum == REGAR ||
			pedidoNum == COSECHAR ||
			pedidoNum == FINALIZAR_TURNO);
}

bool posicionValida(long int i){
	return (i >= 0 && i < N);
}

bool posDentroDeGranja(long int i, long int j){
	return posicionValida(i) && posicionValida(j);
}


bool validar_cultivo(std::string* cultivoAux){
	if(cultivoAux->compare("a") == 0 || cultivoAux->compare("A") == 0 ){
		*cultivoAux = "A";
		return true;
	}
	else if(cultivoAux->compare("b") == 0 || cultivoAux->compare("B") == 0 ){
		*cultivoAux = "B";
		return true;
	}
	else if(cultivoAux->compare("c") == 0 || cultivoAux->compare("C") == 0){
		*cultivoAux = "C";
		return true;
	}
	return false;
}

bool datosSembrarValidos(std::string* cultivoAux, std::string iAux , std::string jAux, long int* i , long int* j){
	return validar_num((char*)iAux.c_str(), i) &&
			validar_num((char*)jAux.c_str(), j) &&
			posDentroDeGranja(*i, *j) &&
			validar_cultivo(cultivoAux);

}

bool datosGranjaValidos( std::string iAux , std::string jAux, long int* i , long int* j){
	return validar_num((char*)iAux.c_str(), i) &&
			validar_num((char*)jAux.c_str(), j) &&
			posDentroDeGranja(*i, *j);
}



