/*
 * cultivo.h
 *
 *  Created on: 02/04/2018
 *      Author: patron
 */

#ifndef CULTIVO_H_
#define CULTIVO_H_

#include <string>


typedef struct Cultivo{
	char nombre;
	int costoSemilla;
	int tiempoCosecha;
	int rentabilidad;
	int tiempoDeRecup;

}cultivo_t;


char cultivoVerNombre(cultivo_t cultivo);


int cultivoVerCostoSemilla(cultivo_t cultivo);




int cultivoVerTiempoRecuperacion(cultivo_t cultivo);



int cultivoVerTiempoParaCosecha(cultivo_t cultivo);



int cultivoVerRentabilidad(cultivo_t cultivo);



bool cultivoDividirTiempoDeRecuperacion(cultivo_t cultivo);




#endif /* CULTIVO_H_ */
