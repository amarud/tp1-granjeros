/*
 * cultivo.cpp
 *
 *  Created on: 04/04/2018
 *      Author: patron
 */

#include "cultivo.h"



char cultivoVerNombre(cultivo_t cultivo){
	return cultivo.nombre;
}

int cultivoVerCostoSemilla(cultivo_t cultivo){
	return cultivo.costoSemilla;
}

int cultivoVerTiempoRecuperacion(cultivo_t cultivo){
	return cultivo.tiempoDeRecup;
}

int cultivoVerTiempoParaCosecha(cultivo_t cultivo){
	return cultivo.tiempoCosecha;
}

int cultivoVerRentabilidad(cultivo_t cultivo){
	return cultivo.rentabilidad;
}




