/*
 * accionesDisponibles.cpp
 *
 *  Created on: 04/04/2018
 *      Author: patron
 */

#include "jugador.h"
#include "validaciones.h"
#include "accionesDisponibles.h"

using namespace std;


int obtenerPrecioCultivo(std::string cultivoAct){
	if(cultivoAct.compare("A") == 0)
		return COSTO_CULTIVO_A;
	else if(cultivoAct.compare("B") == 0)
		return COSTO_CULTIVO_B;
	else
		return COSTO_CULTIVO_C;

}


/** OBS: defino que al sembrar un cultivo, ya está siendo regada.
 *
 */

void sembrar(jugador_t* jugador, bool* okPedido ){
	std::string iAux;
	std::string jAux;
	std::string cultivoAux;
	cout << "ingresa el cultivo <A , B o C> y la posición a colocarlo, separados por espacios:" << endl;
	cout << "(ej: A 1 2 ; coloca el cultivo A en la posicion (1,2) : ";
	cin >> cultivoAux >> iAux >> jAux;
	long int i = -1;
	long int j = -1;
	if( datosSembrarValidos(&cultivoAux, iAux, jAux, &i, &j)){
		i--; j--; // corrimiento según tablero interno.
		if( !parcelaEstaOcupada( jugadorVerParcela(jugador, i, j) ) ){
			if( creditosSuficientes(jugador, cultivoAux) ){
					cout << cultivoAux << endl;
					jugadorSembrarCultivo(jugador, i, j, cultivoAux);
					return;
			}
			else{
				*okPedido = false;
			}
		}
		else{
			*okPedido = false;
		}
	}
	else{
		*okPedido = false;
	}
}



void regar(jugador_t* jugador, bool* okPedido){
	std::string iAux;
	std::string jAux;

	cout << "ingresa la posición a regar, separados por espacios:" << endl;
	cout << "(ej: 1 2 ; utiliza una unidad de riego en la posicion (1,2) : ";
	cin >> iAux >> jAux;
	long int i = -1;
	long int j = -1;
	if( datosGranjaValidos(iAux, jAux, &i, &j)){
		i--; j--; // corrimiento según tablero interno.
		if( parcelaEstaOcupada( jugadorVerParcela(jugador, i, j) ) ){
			if( jugadorVerUnidadesRiego(jugador) > 0 ){
				jugadorRegarCultivo(jugador, i ,j);
				return;
			}
			else{
				*okPedido = false;
			}
		}
		else{
			*okPedido = false;
		}
	}
	else{
		*okPedido = false;
	}
}


void cosechar(jugador_t* jugador, bool* okPedido){
	std::string iAux;
	std::string jAux;

	cout << "ingresa la posición a cosechar, separados por espacios:" << endl;
	cout << "(ej: 1 2 ; cosecha en la posicion (1,2) : ";
	cin >> iAux >> jAux;
	long int i = -1;
	long int j = -1;
	if( datosGranjaValidos(iAux, jAux, &i, &j)){
		i--; j--; // corrimiento según tablero interno.
		if( parcelaEstaOcupada( jugadorVerParcela(jugador, i, j) ) ){
			if( cultivoVerTiempoParaCosecha(jugador->granja[i][j].cultivo) == 0 ){
				jugadorCosecharCultivo(jugador, i, j);
				return;
			}
			else{
				*okPedido = false;
			}
		}
		else{
			*okPedido = false;
		}
	}
	else{
		*okPedido = false;
	}
}


void resetearRegados(jugador_t* jugador){
	for (int i=0; i < N; i++) {
			for (int j=0; j < N; j++) {
				if ( parcelaEstaOcupada(jugador->granja[i][j]) )
					jugador->granja[i][j].regada = false;
			}
		}
}


/* Aquí defino que si sobran unidades de riego que exceden la capacidad del tanque,
 * estas son desechadas, quedando el tanque lleno en el proximo turno.
 */
void jugadorAlmacenarUnidadesRestantes(jugador_t* jugador){
	if(jugadorVerUnidadesRiego(jugador) > CAPACIDAD_TANQUE)
		jugador->unidadesRiegoTanque = CAPACIDAD_TANQUE;
}

void actualizarTiemposCultivo(jugador_t* jugador){
	for (int i=0; i < N; i++) {
		for (int j=0; j < N; j++) {
			if ( parcelaEstaOcupada(jugador->granja[i][j]) ){
				if(cultivoVerTiempoParaCosecha(jugador->granja[i][j].cultivo) > 0){
					if(!parcelaFueRegada(jugador->granja[i][j])){
						jugador->granja[i][j].cultivo.tiempoCosecha = 0;
					}
					else{
						jugador->granja[i][j].cultivo.tiempoCosecha--;
						if(jugador->granja[i][j].cultivo.tiempoCosecha == 0)
							jugador->granja[i][j].podrida = true;
					}
				}
				else if(cultivoVerTiempoParaCosecha(jugador->granja[i][j].cultivo) == 0){
					//3. Fue cosechado a tiempo, curso normal de recuperación.
					if (parcelaFueCosechada(jugador->granja[i][j])){
						jugador->granja[i][j].cultivo.tiempoDeRecup--;
					}
					//2. No logró ser cosechado a tiempo.
					else if( !parcelaFueCosechada(jugador->granja[i][j]) ){
						if(parcelaEstaPodrida(jugador->granja[i][j]) ){
							jugador->granja[i][j].cultivo.tiempoDeRecup /= COF_DE_RECUPERACION;
						}
						else{
							jugador->granja[i][j].cultivo.tiempoDeRecup--;
						}
					}
					//4. liberar parcela recuperada.
					if(cultivoVerTiempoRecuperacion(jugador->granja[i][j].cultivo) == 0){
						jugador->granja[i][j].ocupada = false;
						jugador->granja[i][j].cosechada = false;
					}
				}
			}
		}
	}
}

void finalizarTurno(jugador_t* jugador){
	actualizarTiemposCultivo(jugador);
	resetearRegados(jugador);
	jugadorAlmacenarUnidadesRestantes(jugador);
	jugador->tiempoJuego--;
}


