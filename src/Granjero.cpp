/*
 * Granjero.cpp
 *
 *  Created on: 04/04/2018
 *      Author: patron
 */


#include <iostream>
#include <cstdlib>
#include "jugador.h"
#include "validaciones.h"
#include "accionesDisponibles.h"

const char* MENSAJE_OK = "OK";
const char* MENSAJE_SOLICITAR_ACCION = "ingresa un número según tu elección: ";
const char* MENSAJE_ERROR_DE_PEDIDO = "tu opción no es válida, ingresar números entre 1-4 inclusive";
const char* MENSAJE_ERROR_DE_PROCESO = "error en pedido:";


void jugadorCargarCondicionesIniciales(jugador_t*);

using namespace std;

/* estados de cultivos de granja */
const char CAMPO_LIBRE = 'O';

void ver_ref_granja_debajo(){
	cout << endl << "   ";
		for (int num = 1; num < N+1; num++) {
			cout << num << " ";
		}
	cout << endl;
}

void mostrarGranja(jugador_t* jugador){
	for (int i=0; i < N; i++) {
		cout << endl;
		cout << i + 1 << "  ";
			for (int j=0; j < N; j++) {
				if(parcelaEstaOcupada(jugador->granja[i][j])){
					cultivo_t cultivo = jugador->granja[i][j].cultivo;
					char nombre = cultivoVerNombre(cultivo);
						cout << nombre << " " ;
					}
				if( !parcelaEstaOcupada(jugador->granja[i][j]) ){
							cout << CAMPO_LIBRE << " ";
						}
			}
		}
	ver_ref_granja_debajo();
}



void imprimirOpcionesJuego(){
	cout << "1. Comprar y sembrar semilla" << endl;
	cout << "2. Regar Parcela" << endl;
	cout << "3. Cosechar Parcela" << endl;
	cout << "4. Finalizar Turno" << endl << endl;
}

void imprimirEstadoJuego(jugador_t* jugador){
	int creditosAct = jugadorVerCreditos(jugador);
	int tiempoRestante = jugadorVerTiempoJuego(jugador);
	int unidadesRiego = jugadorVerUnidadesRiego(jugador);
	cout << "Creditos restantes: " << creditosAct;
	cout << " Turnos restantes: " << tiempoRestante << endl;
	cout << "Unidades de Riego: " << unidadesRiego << endl;
}

void imprimirEstadoCultivos(jugador_t* jugador){
	for (int i=0; i < N; i++) {
				for (int j=0; j < N; j++) {
					if ( parcelaEstaOcupada(jugador->granja[i][j]) ){
						cultivo_t cultivo = jugador->granja[i][j].cultivo;
						int tiempoCosecha = cultivoVerTiempoParaCosecha(cultivo);
						int rentabilidad = cultivoVerRentabilidad(cultivo);
						int tiempoRecup = cultivoVerTiempoRecuperacion(cultivo);
						cout << "El cultivo (" << i << ", "<< j << "posee: "<< endl;
						cout << "Tiempo Cosecha: " << tiempoCosecha << " Rentabilidad: " << rentabilidad << endl;
						cout << "Tiempo de Recup: " << tiempoRecup << endl;
					}
				}
			}
}


bool creditosSuficientes(jugador_t* jugador, std::string cultivoAct){
	if( jugadorVerCreditos(jugador) > obtenerPrecioCultivo(cultivoAct) ){
		return true;
	}
	return false;
}


bool obtenerPedido(long int* pedidoNum){
	string aux;
	imprimirOpcionesJuego();
	cout << MENSAJE_SOLICITAR_ACCION;
	cin >> aux;
	if(validar_num( (char*)aux.c_str(), pedidoNum))
		return true;
	return false;
}

void procesarPedido(jugador_t *jugador, long int* pedidoNum, bool* okPedido){
	if(*pedidoNum == SEMBRAR){
		sembrar(jugador, okPedido);
	}
	else if (*pedidoNum == REGAR){
		regar(jugador, okPedido);
	}
	else if(*pedidoNum == COSECHAR){
		cosechar(jugador, okPedido);
	}
	else if(*pedidoNum == FINALIZAR_TURNO){
		finalizarTurno(jugador);
	}

}


int main() {
	jugador_t jugador;
	jugadorCargarCondicionesIniciales(&jugador);

	while (jugadorVerTiempoJuego(&jugador) > FIN_DEL_JUEGO){
		jugadortirarDado(&jugador);
		mostrarGranja(&jugador);
		long int pedidoNum = 0;
		imprimirEstadoJuego(&jugador);
		imprimirEstadoCultivos(&jugador);
		if(obtenerPedido(&pedidoNum) && pedidoEsValido(pedidoNum)){
			bool okPedido = true;
			procesarPedido(&jugador, &pedidoNum, &okPedido);
			if(okPedido){
				cout << MENSAJE_OK << endl;
				continue;
			}
			else
				cout << MENSAJE_ERROR_DE_PROCESO << pedidoNum << endl;
		}
		else{
			cout << MENSAJE_ERROR_DE_PEDIDO << endl;
		}

	}

	return 0;
}
