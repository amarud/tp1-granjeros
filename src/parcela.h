/*
 * parcela.h
 *
 *  Created on: 02/04/2018
 *      Author: patron
 */

#ifndef PARCELA_H_
#define PARCELA_H_

#include "cultivo.h"



typedef struct Parcela{
	bool ocupada;
	bool regada;
	bool cosechada;
	bool podrida;
	cultivo_t cultivo;
} parcela_t;


bool parcelaEstaOcupada(parcela_t parcela);


bool parcelaFueRegada(parcela_t parcela);

bool parcelaFueCosechada(parcela_t parcela);

bool parcelaEstaPodrida(parcela_t parcela);


#endif /* PARCELA_H_ */
