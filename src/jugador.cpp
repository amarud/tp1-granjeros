/*
 * jugador.cpp
 *
 *  Created on: 02/04/2018
 *      Author: patron
 */

#include "jugador.h"
#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

//parcela_t jugadorVerParcela(jugador_t* jugador, long int i , long int j);

void jugadorInicializarGranja(jugador_t* jugador){
	for (int i=0; i < N; i++) {
		for (int j=0; j < N; j++) {
			jugador->granja[i][j].ocupada = false;
			jugador->granja[i][j].cosechada = false;
		}
	}
}

void jugadorCargarCondicionesIniciales(jugador_t* jugador){
	jugadorInicializarGranja(jugador);
	jugador->creditosActuales = CREDITOS_INICIALES;
	jugador->tiempoJuego = TIEMPO_JUEGO;
	jugador->unidadesRiegoTanque = CAPACIDAD_TANQUE;
}

void jugadortirarDado(jugador_t* jugador){
	int dado = rand() % 6 + 1;
	int unidadesAdquiridas = dado * POTENCIADOR_DE_AGUA;
	cout << "En este turno has conseguido un potenciador de agua de:" << dado << endl;
	jugador->unidadesRiegoTanque += unidadesAdquiridas;
}

parcela_t jugadorVerParcela(jugador_t* jugador, long int i , long int j){
	return jugador->granja[i][j];
}

int jugadorVerCreditos(jugador_t* jugador){
	return jugador->creditosActuales;
}

int jugadorVerTiempoJuego(jugador_t* jugador){
	return jugador->tiempoJuego;
}

int jugadorVerUnidadesRiego(jugador_t* jugador){
	return jugador->unidadesRiegoTanque;
}



bool jugadorSembrarCultivo(jugador_t* jugador,long int i, long int j, std::string nombreCultivo){
	if(nombreCultivo.compare("A") == 0){
		jugador->granja[i][j].cultivo.nombre = 'A';
		jugador->granja[i][j].cultivo.costoSemilla = COSTO_CULTIVO_A;
		jugador->granja[i][j].cultivo.rentabilidad = RENTABILLIDAD_A;
		jugador->granja[i][j].cultivo.tiempoCosecha = TIEMPO_DE_COSECHA_A;
		jugador->granja[i][j].cultivo.tiempoDeRecup = TIEMPO_RECUPERACION_A;

	}
	else if (nombreCultivo.compare("B") == 0){
		jugador->granja[i][j].cultivo.nombre = 'B';
	    jugador->granja[i][j].cultivo.costoSemilla = COSTO_CULTIVO_B;
	    jugador->granja[i][j].cultivo.rentabilidad = RENTABILLIDAD_B;
	    jugador->granja[i][j].cultivo.tiempoCosecha = TIEMPO_DE_COSECHA_B;
		jugador->granja[i][j].cultivo.tiempoDeRecup = TIEMPO_RECUPERACION_B;
	}
	else if (nombreCultivo.compare("C") == 0){
		jugador->granja[i][j].cultivo.nombre = 'C';
		jugador->granja[i][j].cultivo.costoSemilla = COSTO_CULTIVO_C;
		jugador->granja[i][j].cultivo.rentabilidad = RENTABILLIDAD_C;
		jugador->granja[i][j].cultivo.tiempoCosecha = TIEMPO_DE_COSECHA_C;
		jugador->granja[i][j].cultivo.tiempoDeRecup = TIEMPO_RECUPERACION_C;
	}
	else{
		return false;
	}
	jugador->granja[i][j].ocupada = true;
	jugador->granja[i][j].regada = true;
	jugador->granja[i][j].podrida = false;
	jugador->creditosActuales -= cultivoVerCostoSemilla(jugador->granja[i][j].cultivo);
	return true;
}


void jugadorRegarCultivo(jugador_t* jugador,long int i , long int j){
	jugador->granja[i][j].regada = true;
	jugador->unidadesRiegoTanque -= 1;
}

void jugadorCosecharCultivo(jugador_t* jugador,long int i,long int j){
	jugador->creditosActuales += jugador->granja[i][j].cultivo.rentabilidad;
	jugador->granja[i][j].cosechada = true;
}



