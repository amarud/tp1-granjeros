/*
 * parcela.cpp
 *
 *  Created on: 02/04/2018
 *      Author: patron
 */

#include "parcela.h"

bool parcelaEstaOcupada(parcela_t parcela){
	return parcela.ocupada == true;
}

bool parcelaFueRegada(parcela_t parcela){
	return parcela.regada == true;
}

bool parcelaFueCosechada(parcela_t parcela){
	return parcela.cosechada;
}

bool parcelaEstaPodrida(parcela_t parcela){
	return parcela.podrida == true;
}



