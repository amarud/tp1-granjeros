/*
 * validaciones.h
 *
 *  Created on: 04/04/2018
 *      Author: patron
 */

#ifndef VALIDACIONES_H_
#define VALIDACIONES_H_


/* acciones lógicas del juego */
const long int SEMBRAR = 1;
const long int REGAR = 2;
const long int COSECHAR = 3;
const long int FINALIZAR_TURNO = 4;

bool validar_num(char* str, long int* num);



bool pedidoEsValido(long int pedidoNum);



bool posDentroDeGranja(long int i, long int j);


bool datosSembrarValidos(std::string* cultivoAux, std::string iAux , std::string jAux, long int* i , long int* j);


bool datosGranjaValidos( std::string iAux , std::string jAux, long int* i , long int* j);


bool creditosSuficientes(jugador_t* jugador, std::string cultivoAct);




#endif /* VALIDACIONES_H_ */
