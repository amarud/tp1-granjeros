/*
 * jugador.h
 *
 *  Created on: 02/04/2018
 *      Author: patron
 */

#ifndef JUGADOR_H_
#define JUGADOR_H_

#include <iostream>
#include <string>
#include "parcela.h"

const int COF_DE_RECUPERACION = 2;


/*Datos Cultivo A*/
const int COSTO_CULTIVO_A = 1;
const int TIEMPO_DE_COSECHA_A = 2;
const int RENTABILLIDAD_A = 10;
const int TIEMPO_RECUPERACION_A = 1;

/* Cultivo B*/
const int COSTO_CULTIVO_B = 3;
const int TIEMPO_DE_COSECHA_B = 4;
const int RENTABILLIDAD_B = 30;
const int TIEMPO_RECUPERACION_B = 2;

/*Cultivo C*/
const int COSTO_CULTIVO_C = 2;
const int TIEMPO_DE_COSECHA_C = 1;
const int RENTABILLIDAD_C = 5;
const int TIEMPO_RECUPERACION_C = 0;

/* condiciones iniciales de juego */
const int CREDITOS_INICIALES = 50;
const int TIEMPO_JUEGO = 10;
const int FIN_DEL_JUEGO = 0;
const int N = 5; // Tamaño de una granja de NxN.
const int CAPACIDAD_TANQUE = 25; // agua.
const int POTENCIADOR_DE_AGUA = 5;

/*definición de la estructura lógica del jugador*/
typedef struct Jugador{
	parcela_t granja[N][N];
	int creditosActuales;
	int unidadesRiegoTanque;
	int tiempoJuego;
}jugador_t;



void jugadortirarDado(jugador_t* jugador);



void jugadorCargarCondicionesIniciales(jugador_t* jugador);



bool jugadorSembrarCultivo(jugador_t* jugador,long int i, long int j, std::string nombreCultivo);



void jugadorRegarCultivo(jugador_t* jugador,long int i , long int j);



void jugadorCosecharCultivo(jugador_t* jugador,long int i,long int j);


parcela_t jugadorVerParcela(jugador_t* jugador, long int i , long int j);



int jugadorVerCreditos(jugador_t* jugador);



int jugadorVerTiempoJuego(jugador_t* jugador);



int jugadorVerUnidadesRiego(jugador_t* jugador);








#endif /* JUGADOR_H_ */
