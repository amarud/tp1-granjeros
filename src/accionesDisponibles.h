/*
 * accionesDisponibles.h
 *
 *  Created on: 04/04/2018
 *      Author: patron
 */

#ifndef ACCIONESDISPONIBLES_H_
#define ACCIONESDISPONIBLES_H_


#include <iostream>
#include <string>



int obtenerPrecioCultivo(std::string cultivoAct);




void sembrar(jugador_t* jugador, bool* okPedido );




void regar(jugador_t* jugador, bool* okPedido);




void cosechar(jugador_t* jugador, bool* okPedido);



void finalizarTurno(jugador_t* jugador);



#endif /* ACCIONESDISPONIBLES_H_ */
